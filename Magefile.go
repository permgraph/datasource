//+build mage

package main

import (
	"fmt"
	"github.com/magefile/mage/sh"
	"os"
	"os/exec"

	//mage:import
	"github.com/grafana/grafana-plugin-sdk-go/build"
)

func RunGrafana() error {
	var (
		podmanBinaryPath string
		workingDir       string
		err              error
	)

	if workingDir, err = os.Getwd(); err != nil {
		return err
	}
	if podmanBinaryPath, err = exec.LookPath("podman"); err != nil {
		return err
	}
	return sh.RunV(
		podmanBinaryPath,
		"run",
		"-v", fmt.Sprintf("%s:/var/lib/grafana/plugins/perm-graph", workingDir),
		"-p", "3000:3000",
		"-e", "GF_PLUGINS_ALLOW_LOADING_UNSIGNED_PLUGINS=perm-graph-datasource",
		"-e", "GF_SECURITY_ADMIN_USER=admin",
		"-e", "GF_SECURITY_ADMIN_PASSWORD=4dm1n",
		"docker.io/grafana/grafana:7.5.7",
	)
}

// Default configures the default target.
var Default = build.BuildAll
