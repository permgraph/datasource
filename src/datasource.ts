import { DataSourceInstanceSettings } from '@grafana/data';
import { DataSourceWithBackend } from '@grafana/runtime';
import { PermGraphDataSourceOptions, MyQuery } from './types';

export class DataSource extends DataSourceWithBackend<MyQuery, PermGraphDataSourceOptions> {
  constructor(instanceSettings: DataSourceInstanceSettings<PermGraphDataSourceOptions>) {
    super(instanceSettings);
  }
}
