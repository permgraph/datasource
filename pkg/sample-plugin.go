package main

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/google/uuid"
	"github.com/grafana/grafana-plugin-sdk-go/backend"
	"github.com/grafana/grafana-plugin-sdk-go/backend/instancemgmt"
	"github.com/grafana/grafana-plugin-sdk-go/backend/log"
	"github.com/grafana/grafana-plugin-sdk-go/data"
)

// Make sure PermGraphDataSource implements required interfaces. This is important to do
// since otherwise we will only get a not implemented error response from plugin in
// runtime. In this example datasource instance implements backend.QueryDataHandler,
// backend.CheckHealthHandler, backend.StreamHandler interfaces. Plugin should not
// implement all these interfaces - only those which are required for a particular task.
// For example if plugin does not need streaming functionality then you are free to remove
// methods that implement backend.StreamHandler. Implementing instancemgmt.InstanceDisposer
// is useful to clean up resources used by previous datasource instance when a new datasource
// instance created upon datasource settings changed.
var (
	_ backend.QueryDataHandler      = (*PermGraphDataSource)(nil)
	_ backend.CheckHealthHandler    = (*PermGraphDataSource)(nil)
	_ instancemgmt.InstanceDisposer = (*PermGraphDataSource)(nil)
)

// NewPermGraphDataSource creates a new datasource instance.
func NewPermGraphDataSource(settings backend.DataSourceInstanceSettings) (instancemgmt.Instance, error) {
	var (
		ok bool
	)
	ds := &PermGraphDataSource{
		url: settings.URL,
	}

	if ds.apiKey, ok = settings.DecryptedSecureJSONData["apiKey"]; !ok {
		return nil, errors.New("API key is missing")
	}

	return ds, nil
}

// PermGraphDataSource is an example datasource which can respond to data queries, reports
// its health and has streaming skills.
type PermGraphDataSource struct {
	apiKey string
	url    string
}

// Dispose here tells plugin SDK that plugin wants to clean up resources when a new instance
// created. As soon as datasource settings change detected by SDK old datasource instance will
// be disposed and a new one will be created using NewPermGraphDataSource factory function.
func (d *PermGraphDataSource) Dispose() {
	// Clean up datasource instance resources.
}

// QueryData handles multiple queries and returns multiple responses.
// req contains the queries []DataQuery (where each query contains RefID as a unique identifier).
// The QueryDataResponse contains a map of RefID to the response for each query, and each response
// contains Frames ([]*Frame).
func (d *PermGraphDataSource) QueryData(ctx context.Context, req *backend.QueryDataRequest) (*backend.QueryDataResponse, error) {
	log.DefaultLogger.Info("QueryData called", "request", req)

	// create response struct
	response := backend.NewQueryDataResponse()

	// loop over queries and execute them individually.
	for _, q := range req.Queries {
		res := d.query(ctx, req.PluginContext, q)

		// save the response in a hashmap
		// based on with RefID as identifier
		response.Responses[q.RefID] = res
	}

	return response, nil
}

type queryModel struct {
	WithStreaming bool `json:"withStreaming"`
}

type kind uint8

func (k kind) String() string {
	switch k {
	case kindUser:
		return "User"
	case kindGroup:
		return "group"
	default:
		return ""
	}
}

const (
	kindUser kind = iota
	kindGroup
)

type userOrGroup struct {
	ID       string
	Name     string
	Kind     kind
	MemberOf []userOrGroup
}

func (d *PermGraphDataSource) query(_ context.Context, _ backend.PluginContext, query backend.DataQuery) backend.DataResponse {
	response := backend.DataResponse{}

	// Unmarshal the JSON into our queryModel.
	var qm queryModel

	response.Error = json.Unmarshal(query.JSON, &qm)
	if response.Error != nil {
		return response
	}

	var adminGroup = userOrGroup{
		ID:   uuid.New().String(),
		Name: "Admins",
		Kind: kindGroup,
	}

	var entities = []userOrGroup{
		{
			ID:   uuid.New().String(),
			Name: "Armin.vonBuren",
			Kind: kindUser,
			MemberOf: []userOrGroup{
				adminGroup,
			},
		},
		{
			ID:   uuid.New().String(),
			Name: "ted.tester",
			Kind: kindUser,
			MemberOf: []userOrGroup{
				adminGroup,
				{
					ID:   uuid.New().String(),
					Name: "Testers",
					Kind: kindGroup,
				},
			},
		},
	}

	// add the frames to the response.
	response.Frames = append(response.Frames, nodesFrame(entities)...)

	return response
}

// CheckHealth handles health checks sent from Grafana to the plugin.
// The main use case for these health checks is the test button on the
// datasource configuration page which allows users to verify that
// a datasource is working as expected.
func (d *PermGraphDataSource) CheckHealth(_ context.Context, req *backend.CheckHealthRequest) (*backend.CheckHealthResult, error) {
	log.DefaultLogger.Info("CheckHealth called", "request", req)

	var status = backend.HealthStatusOk
	var message = "Data source is working"

	/*if rand.Int()%2 == 0 {
		status = backend.HealthStatusError
		message = "randomized error"
	}*/

	return &backend.CheckHealthResult{
		Status:  status,
		Message: message,
	}, nil
}

func nodesFrame(entities []userOrGroup) []*data.Frame {
	var (
		nodes *data.Frame
		edges *data.Frame
	)
	nodes = data.NewFrame("nodes")
	nodes.Fields = append(nodes.Fields, data.NewField("id", nil, []string{}))
	nodes.Fields = append(nodes.Fields, data.NewField("title", nil, []string{}))
	nodes.Fields = append(nodes.Fields, data.NewField("mainStat", nil, []string{}))
	nodes.Fields = append(nodes.Fields, colorFields("green", "blue")...)
	nodes.SetMeta(&data.FrameMeta{
		PreferredVisualization: data.VisTypeNodeGraph,
	})

	edges = data.NewFrame("edges")
	edges.Fields = append(edges.Fields, data.NewField("id", nil, []string{}))
	edges.Fields = append(edges.Fields, data.NewField("source", nil, []string{}))
	edges.Fields = append(edges.Fields, data.NewField("target", nil, []string{}))
	edges.Fields = append(edges.Fields, data.NewField("mainStat", nil, []string{}))
	edges.SetMeta(&data.FrameMeta{
		PreferredVisualization: data.VisTypeNodeGraph,
	})

	for idx := range entities {
		appendEntity(entities[idx], nodes, edges)
	}

	return []*data.Frame{nodes, edges}
}

func appendEntity(entity userOrGroup, nodes *data.Frame, edges *data.Frame) {
	switch entity.Kind {
	case kindUser:
		nodes.AppendRow(entity.ID, entity.Kind.String(), entity.Name, 1.0, 0.0)
	case kindGroup:
		nodes.AppendRow(entity.ID, entity.Kind.String(), entity.Name, 0.0, 1.0)
	}

	for _, member := range entity.MemberOf {
		edges.AppendRow(uuid.New().String(), entity.ID, member.ID, "MemberOf")
		appendEntity(member, nodes, edges)
	}
}

func colorFields(userColor, groupColor string) []*data.Field {
	userColorField := data.NewField("arc__user", nil, []float64{})
	userColorField.SetConfig(&data.FieldConfig{
		Color: map[string]interface{}{
			"fixedColor": userColor,
			"mode":       "fixed",
		},
	})

	groupColorField := data.NewField("arc__group", nil, []float64{})
	groupColorField.SetConfig(&data.FieldConfig{
		Color: map[string]interface{}{
			"fixedColor": groupColor,
			"mode":       "fixed",
		},
	})

	return []*data.Field{userColorField, groupColorField}
}
