module gitlab.com/permgraph/datasource

go 1.16

require (
	github.com/google/uuid v1.1.2
	github.com/grafana/grafana-plugin-sdk-go v0.99.0
	github.com/magefile/mage v1.11.0
)
